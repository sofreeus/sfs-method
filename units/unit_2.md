# The Time Pie

## Unit Learning Objectives

- What is The Time Pie?
- When/why do you use it?
- Make your own!

---

## Lesson


### What is The Time Pie?

<img src="/images/time-pie.png" height="400" width="400">

- A tool that aids in class planning.
- A tool that aids in class delivery.
- Three phases:
    1. Lecture: 15 minutes (NEVER more than 22!)
    2. Demo: 15 minutes
    3. Pair & Share: 30 minutes
- Clear progression: start and end


### When/why do you use it?

- When planning a class:
    - Atomic lessons: One Time Pie per lesson
    - Small, tangible chunks:
        - Are easier to plan (15 minutes of talking is easier than 4 hours)
        - Are easier to rehearse
    - Forces deconstruction of larger topics

- When delivering a class:
    - Sets expectations/routine
    - Addresses different learning styles (if you still believe)
    - Leverages [retrieval practice](https://www.learningscientists.org/blog/2024/3/7/how-does-retrieval-improve-new-learning) for improved retention


### How do you use it?

For context, we're going to explore overall class flow in the next unit.  But this lesson is first, because The Time Pie is more important.  It's a building block that helps craft individual lessons that fit well in the broader class.

#### Step 1: The End(s)

First, identify the goal:

```

What is the next (most important) thing for students to know?

```

- Ideally, you want to identify the next most important thing for students to know.  Sometimes an intermediate step is required.

Second: identify the starting point:

- If this is the first lesson in a class, define your starting point via class requirements/prerequisites.  Assume not everyone will come prepared.
- If this is _not_ the first lesson in a class, you're lucky!  You have defined your starting point with the end of the last lesson!
- Either way, be deliberate.
- You can even add a bit to the start of a lesson to further define where students should be to proceed with this lesson.
  - For example:
      >To start this lab, you should have a Docker container running locally on port 8080.  You should be able to access `localhost:8080` from your web browser.

#### Step 2: Contextualize for Curiosity

Your students are curious (but sometimes you might have to dig).  The following concepts don't necessarily translate 1:1 to course materials, but they really help contextualize the lesson while you - the instructor to be - are working through it.

*What enhances curiosity?*

>Making something exciting.  
>Making something interesting.  
>Communicating that this knowledge will make students' lives better. 

*What satisfies curiosity?*

Think like an investigative journalist or detective: `Who?` `What?`  `Where?` `When?` `Why?` `How?`

```
What
  Introduction/overview of the topic.

Why
  The reason for doing this.  The benefit.  Describe how this makes life better.

Who
  Identify the beneficiary.  For the student, it could be: self, customers, boss (so... self)
```
⬆What's above can inform the intro.⬆

⬇What's below can inform the body.⬇

```
When
  Define the point where this knowledge should be employed.

Where
  Server?  Desktop?  Cloud?  Frankly, you can leave this one out most of the time.

How
  Usually the body of the lesson.  Describe and demonstrate how the thing is done.
```


#### Step 3: Fill in the Blanks

You have a starting point, ending point, and a bunch of context.  Start putting pen to paper and documenting the steps required to get from A to B.  This should usually be a numbered series of steps with code, screenshots, gifs, or cat videos that walk students through the task.

Develop and edit the material you'll use during the talk. This can be slides, notes, props, slides with notes, slides with props, whatever is appropriate to your delivery style and audience.

Design your demo and lab. The demo illustrates the objectives of the lesson (because a picture is worth 1k (or is it 1K?) words). The lab is where the students retrieve the objectives by performing an activity. Not all objectives can be demonstrated and not all can be performed in an inherently interesting way. So you may need to add lab steps that foster retrieval and then weave those into your demo.

For example, Daphne's employees need to know about food safety:
- Sanitizing the equipment can be demonstrated/performed.
- Inspecting expiration labels can be demonstrated/performed.
- When/how often equipment is sanitized or inspected and how you decide it's time to do so may not be as easy. What are some ways you can help students retrieve these kinds of objectives?

Without SFS Method, many technical teachers struggle with this.  There's a strong inclination to try and do everything all at once.  With SFS Method, you've identified a bite-sized chunk to work on.  So - hopefully - this will be the easy part.


#### Steps 4-15: Rehearse

More about this in Unit 4.

Most importantly:
- Can you get your talk done in 15-20 minutes?
- Can you get your demo done in 15-20 minutes?

If the answer to either of those questions is "no," you need to break your lesson in to smaller pieces.


#### Delivery

The Time Pie helps tremendously with delivery!  It's actually pretty simple, but it takes intention and discipline.

1. Communicate the format to students at the start of class.
    - Set the expectation that they _do not_ work during the talk or demo sections.
    - This helps to keep "what" or "why" questions during the talk/demo sections, while "how" and "I'm stuck" interactions usually wait to the pair & share segment.
2. Use a timer (if you need to).
    - Be honest with yourself.  If you're prone to ramble or go down a tangent about umask, don't choose to believe that you'll stay on track next time.
3. Don't sweat it.
    - Running too short isn't a problem.  Many of the best classes finish with time to turn down weird, esoteric, or interesting alleys where the teacher is experimenting live.
    - Running long helps inform the next delivery of the class.  Just take notes and take action!
    - Especially in larger classes, the pair & share section can exceed the 30 minutes.
4. Prioritize on-task questions over tangential or hypothetical questions.

----

### Demo

Daphne's course for new managers needs to include a lesson about food safety standards.  Let's walk through creating a lesson using The Time Pie.

- The End(s)
- Contextualize for Curiosity
- Fill in the Blanks
- Rehearse
- Delivery

----

### Lab

Create a single, high-level lesson plan for your course.  You will have completed this lab when you have completed the following:

- What is the learning goal for the lesson?
- Who/what/where/when/why/how?
- Without going in to specifics, what are the high-level steps?
- Rehearse:
  - Can you explain this in 15-20 minutes?
  - Can you demo this in 15-20 minutes?
- Run through your lesson with your partner.
- Run through your lesson with the class.

---

<center>[prev](unit_1.md) | [next](unit_3.md)</center>