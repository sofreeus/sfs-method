# How to Introduce a Person

Making a Great First Impression

## Talk

![the introduction](images/meet.gif)

5 minutes

Sometimes, you have to introduce a person: yourself, an honored speaker, your new boy or girlfriend, or your dog.

First impressions are made in seconds or split-seconds, and have indelible effects on the ensuing relationship. In other words, you never get a second chance to make a good first impression.  First impressions *matter*, and you are managing someone else's first impression.

So, it pays to plan those first few critical moments.

What we'll suggest is that you:

- Be warm. Smile, if you can.
- Include the subject's:
  + Name
  + Role and/or Goal(s)
  + Features
- Encourage a response.

Arrange time to discuss with the subject which features should be highlighted.

Examples:
- "Silvia Briscoe is our network forensics with nmap and wireshark teacher. She is also a world-traveler, entrepreneur, and a great active listener."
- "Dr. Jeff Haemer is teaching Bash today. He has an advanced degree and also loves git."
- "This is Zach, my dog. His goal is to steal your food and cuddle you. He is mostly harmless, but he smells like corn-chips."

CAUTION: Don't surprise your subject. If you're planning to highlight a feature in your introduction, tell them.

We'll do this in 4 phases.
- First, this introductory **Talk**.
- Then, a **Demo** where my imaginary friend Z and I will introduce each other.
- Then, you'll **Pair** up with someone and you and your pair will prepare to introduce each other.
- Then, you'll **Share** your introduction of your partner.

If you're in a "pair" of three people, each person introduces one other person. A -> B. B -> C. C -> A.

## Demo

5 minutes

Z and I discuss details of introduction.

Z and I introduce each other.

## Pair and Share

10 minutes

Learners pair or triple up.

Learners prepare to introduce each other: share names, roles and goals, and features.

Each learner introduces their partner.

Next [The Time Pie](the-time-pie.md)
