# How We Learn

The theory of SFS Method

## Unit Learning Objectives

Retrieval drives retention

Reserve time for retrieval

Make it pair

## Questions

Do I want learning to happen in my class or after or ever?

How can I know whether learning has happened?

What causes retention?

## Lesson

Anecdotally, Hear/See/Do/Say has always worked well.

But, the Myth of Learning Styles (VAK[V]: Visual, Auditory, Kinesthetic, [Verbalizer]) [has been debunked](https://www.youtube.com/watch?v=rhgwIhB58PA).

Fortunately, Hear, See, Do, Say works well with the supported-by-evidence theory of [Retrieval Practice](https://www.retrievalpractice.org/why-it-works).

The Time Pie, our reference implementation of SFS Method still works, because, (Hear/See) / (Do/Say) fits the deliver/retrieve model, too!

## Teaching Methods which are "Method"

- EOTO
    - Each One Teach One
- SODOTO
    - Show One, Do One, Teach One
- EDGE (BSA Method)
    - Explain, Demonstrate, Guide, Enhance

## Teaching Methods which are not "Method"

- Lecture & Homework
- TED Talk
- Tech Talk

When implementing the SFS Method as a teacher, I ensure that my learners:

```
HEAR    
SEE
DO
SAY
```

## Demo

[How To Introduce A Person](how-to-introduce-a-person.md)

Introduce Daphne - Daphne has created a coffee shop that has outgrown one barista. She reads "The E Myth Revisited" and realizes that she needs more baristas.

She'll need to do a lot of effective training, but for now, she needs to introduce her new barista to her loyal customer, and teach her barista do do likewise.

## Pair and Share

Teach your partner how to introduce you.

Introduce your partner.

<center>[prev](/README.md) | [next](unit_2.md)</center>