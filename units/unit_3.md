# Prioritization & Flow

<!--
Consider:
- Do you want to leave your teaching engagement with regret?  
- Because it all felt flat?  
- Because you spent too much time on trivial junk and didn't get to the really important thing?

And:
- Do you want to learn Python list generators in a boring standalone lesson about Python list generators?
- Or do you want to learn Python list generators by making a fun Pokémon-style game?
-->

Flow:
- Flow is the "smoothness" of a class.  How well the pieces fit together.  
- Do the lessons build on each other?  Or are they independent and disjointed?  
- Are students working toward an overall goal?  Or are they just learning chunks of information?

Prioritization:
- Prioritization informs flow, but flow informs prioritization, too.
- Sometimes you'll have something in mind that seems really important to teach, but doesn't fit into the flow of the class.
- This usually means it's better saved as an extra or bonus after the body of the class.

## Unit Learning Objectives

You should finish a class thinking, 'There's nothing I could have done differently to give more value than I did.'

- Prioritize and decide with the 7-3-1 approach.
- Maintain a coherent progression throughout your class.
- Understand how to value-ramp your course to deliver the best value.

---

## Lesson

### 7-3-1 Prioritization

`n-7-3-1` - or just `7-3-1` - is a prioritization tool.  When considering a topic for a class, you might be overwhelmed by the number (n) of _super-important_ things you need to teach students.

1. From the cloud of **all the things** you _could_ teach, choose the 7 you think are most important.
2. Now choose the three most important things from your list of 7.  The [Rule of Three](https://en.wikipedia.org/wiki/Rule_of_three_(writing)) (check the refs) is that people engage more effectively with information presented in groups of three.
3. From your list of 3, select the One Thing that students most need to know.

The steps are simple, but this can still be a difficult process.  Going through it will help you identify, rationalize, and discern your priorities.  As you gain confidence in this process, you may find yourself using it outside of class planning.

*KISS*

To paraphrase a Tim Sandlin quote I can't find:

>The thing about clichés is that everybody understands them.

- The more common an application, the better the engagement. 
- The more common the problem, the better the lab. 
- The more common the tools, the easier to learn. 

Additionally:

- Err on the side of too simple 
- Err on the side of too short 
- Err on the side of too small 

The only person who will lament the lack of complexity is you, the teacher.

Students looking for more complexity can ask for it (during lab or otherwise).

It's better to have a student or two asking for more complexity than a student or two following along while all the rest are lost.

### Treasure Map

<img src="/images/treasure-map.png" width="600" height="400">

Treasure maps are such a compelling plot device because they mandate the suspension of disbelief.  You wouldn't go miles off course and solve a dozen riddles to get to WalMart.  But to find the hidden treasures of Wispy Pete, the Anemic Pirate?  Sign me up!  Even if we have to take a seemingly-irrelevant detour, I'm still bought in to the belief that it's all in service of assuming the legacy of Wispy Pete.

A narrative can make a class enjoyable, but the X is more important.  It provides context and motivation.  The _why_ behind what we're doing.  If I tell you Python list generators are important, I'm likely to lose your interest.  If we're making a Pokémon game and list generators facilitate the management of initiative in a multi-monster battle?  You'll probably be much more interested in list generators.  We don't have to have a narrative about Ash and Pikachu; just contextualizing the course under a Pokémon game is sufficient.

As a student, which of the following would you rather complete?

 1. A smattering of disparate labs
 1. A progression of labs that build on each other to yield a kinda cool project
 
 The overall information might be the same, but most students will get a bigger dopamine reward from what they percieve is a larger accomplishment.\*  And an alum telling friends, "I built a web app!" is better marketing than, "Yeah.  I learned a few different things about web apps."\*

\* Not scientific.  Teacher belief.

As a teacher, identifying the X helps scope your class.  Can my students build X in four hours?  No?  Choose a smaller X.  If you think students can build X in four hours, use the Time Pie to start breaking it down into lessons.

You may notice that this is very similar to the [Time Pie Usage](https://gitlab.com/sofreeus/sfs-method/-/blob/master/units/unit_2.md#how-do-you-use-it) instructions: identify the end(s), contextualize, and fill in the blanks.

A process for class development might look like this:

1. Get overwhelmed by teaching "Python"
1. Use the 7-3-1 process to identify the top priorities for student learning. \*
1. Think of an approachable project (Tic-Tac-Toe, Battleship, etc.) that can be completed in the time you have allocated for the class. \*
1. Use The Time Pie to break your project down into manageable lessons.
1. Too big?  Too small?  Are the important topics covered?  Iterate until you're happy.

\* You can do these in any order.

<!--
```
    Key learning topics ---------+
            ^                    |
            |                    v
            +----- The thing we're going to build
```
-->

```mermaid
graph LR
  A{{Key Learning Topics}}
  B{{Project to Build}}
  A-->B
  B-->A
```

### Value-Ramping

- Lead with the highest-value thing first  
- Sometimes an operational issue or dependency will break this 
- Try to make the dependencies smaller 
- Structure lessons in terms of decreasing value in order to cut your losses if you can't cover everything

Note that this is very close to an Agile process for prioritization.  The idea is similar: Get the most valuable features to the customer as quickly as possible.

<!--
Value of Lessons

```
|
|___
|   |
| L |___
| e |   |    ___
| s |   |___|   |    ___
| s |   |   |   |___|   |
| o |   |   |   |   |   |___     ___
| n |   |   |   |   |   |   |___|   |___ ___
|   |   |   |   |   |   |   |   |   |   |   |___
| 1 | 2 | 3 | . | . | . | . | . | . | . | N |   |___
|___|___|___|___|___|___|___|___|___|___|___|___|___|__
```
-->

```mermaid
xychart-beta
    title "Lesson Value Ramping"
    x-axis [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    y-axis "Lesson Value" 1 --> 10
    bar [10, 8, 6, 7, 5, 6, 4, 3, 2, 3, 2, 1]
```

---

## Demo

Python:

- [Language Reference](https://docs.python.org/3/reference/index.html)
- [Built-In Functions](https://docs.python.org/3/library/functions.html)
- [Built-In Types](https://docs.python.org/3/library/stdtypes.html)

<details>
  <summary>First</summary>

* Too many things!
* Pick a project: Pirate Simulator
    * Fights
    * Treasure
    * Pirate skill/trait progression
    * ~~Navigation~~

</details>

<details>
  <summary>Second</summary>

What do students need to know to make a Pirate Simulator?

* Python syntax elements:
    * Function definitions
    * Flow control
    * Operators
* Variables and types:
    * Strings
    * Numbers
    * Dictionaries
* Extras
    * Lambdas
    * Generators
    * Classes

</details>

<details>
  <summary>Third</summary>

Value Ramping:

1. Pirate skills
    - Variable types, setting, and modification
    - Operators
2. Fights
    - Flow control
    - Functions
    - Generators?
3. Treasure
    - Lambdas?
    - Generators?

</details>

----

## Review

### 7-3-1

* No more than seven
* Three is great
* One is enough

### Treasure Map

* Projects engage students more than (just) lessons
* Projects help teachers scope classes
* Iterate to right-size

### Value Ramping

* Put the most important stuff first so you can deliver the most value

----

## Lab

Daphne has identified the following topics for her course:

- janitorial/keeping the café clean
- customer service
- make excellent coffee
- marketing and local promotions
- using the point of sale systems
- inventory management
- food safety standards
- washing dishes
- opening/closing procedure

Tasks:
- Use the 7-3-1 method to prioritize the list (are any missing?)
- Can you improve the Treasure Map?
- How would you value-ramp this?

---

<center>[prev](unit_2.md) | [next](unit_4.md)</center>
