# The SFS-Method Prep

Naive optimism ruins otherwise good classes.

## Talk

![The Prep](images/prep.png)

Preparation takes at least 5 time-units of prep for one time-unit of delivery.

I recommend that you spend them like this:

1. Write for one time-unit

    - Write brief Talks with good visual maps where useful
    - *Rehearse* your activity/exercise/labs
    - Run a QA-checklist against your material
    - For today, we will threat model

2. Rehearse for one time-unit

    - Rubber Duck is good.
    - Human is better.
    - Interested human is best.

3. Debug, polish, write some more.

4. Rehearse one more time.

5. Debug and polish. Bad time to add.

Deliver with energy.

## Demo

Threat model on "how to make a proper cappucinno"

For each threat, is this easy to foresee or hard to foresee?

## Pair & Share

Threat model on your own classes.

For each threat, is it easy to foresee or hard?

Is it likely or unlikely that a meat-duck (a friend) will surface new, unforeseen threats?

Prepare!

Strongly consider using a QA checklist like this:

* Is my One Thing clear? Do I repeat it a few times in slightly different ways?
* Is my example exercise real, hard fun?
* Is the lab env adequately controlled?
* Do my labs encourage pairing?
* Do the Demo and the Pair Lab align?
* Did I ensure that Knowledge Objectives, things they can't *do*, they must *say*?

<center>[prev](unit_3.md) | [next](README.md)</center>