# The SFS-Method Prep

Naive optimism ruins otherwise good classes.

## Talk

< 10 minutes

![The Prep](images/prep.png)

Preparation takes at least 5 time-units of prep for one time-unit of delivery.

I recommend that you spend them like this:

For one time-unit:

- Write brief Talks with good visual maps where useful
- *Try* your activity/exercise/labs
- Run a QA-checklist against your material.

For one time-unit:

* Rehearse your material from end-to-end.
  - Rubber Duck is good.
  - Human is better.
  - Interested human is best.

For one time-unit, write some more.

For one time-unit, rehearse again.

For one time-unit, write some more.

Deliver excellently.

## Demo

< 10 minutes

I will demonstrate how I might start writing out a plan for a class on fixing bicycle tires. I will then rehearse it.

## Pair & Share

20+ minutes

Now pairs, do likewise. Start working together on a unit plan. Do at least one iteration of write, then rehearse. If you have time, do more.

As a pair, deliver your class!
