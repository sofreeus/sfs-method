SFS Method
===

SFS Rap

## Overview

(about 25 minutes)

![Time Pie](images/time_pie.png)

Overview of Time Pie. Go right into intros. (5m)

[How to Introduce a Person](how-to-introduce-a-person.md) (20m+)

## How do we learn?

(15 minutes)

- Learning Styles
- Working Memory
- Verbalization / Re-teaching
- 7/3/1
- Review Time Pie (5 min)

## How to Choose a Topic and Objectives

Choose a topic (7/3/1)

Choose Objectives (7/3/1)

Start an outline (Talk, Demo, Pair & Share)

## How to Plan a Lesson

[Time Pie Lesson Template](time-pie-lesson-template.md)

[How to Prepare a Time Pie](how-to-prepare-a-time-pie.md)

(Group/class review of outlines)

## SFS tools

- GitLab - SFS Method class template project comming soon
- Nextcloud - Share your big files here!
- Big Blue Button - send your witty aphorisms far, far away
- ssh - cloud server, terminal server
- web browser
- Docker
- VirtualBox

## How to Use a VirtualBox Environment

- Vagrant or scratch build
- export to an OVA
- import from an OVA

## Test, test, test your material

Get a friend to go through the unit with you.

## Exam & Review
