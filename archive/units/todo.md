During Talk:

Introduce a tool, feature, or minor skill.

What is it? What does it do? What problem does it solve?

During Demo:

Conjure the most common problem that this feature solves?
Swords need a handle, because holding them by the sharp bit
How do you perfectly solve that problem?
Why do you do it that way?
 do you use it?the *most important* task all students will learn how to do in your class?
Under what circumstances d
What is the the right way to do that, and why? Can you demonstrate it?

Move everything but README.md, plan.md, and images/ into (author)/

Make SFS Method (the course) an instance of SFS Method (the template).
Create an Impress template
Use the Impress template to create slides for SFS Method
Add 'How to Get Ready for (course name)' to template
Add 'How to Get Ready for SFS Method' to SFS Method

SFS Method, part 1, the Time Pie
- free as in beer
- 2 hours
- helps the SME / beginnner teacher to write, test, and deliver a single lesson

SFS Method, parts 2-4, building classes, using tools
- pricing?
- 6 hours
- helps the SME to compose coherent classes of multiple lessons
- helps the beginning SFS teacher to become familiar with our tools:
  + GitLab
  + VirtualBox
  + Docker
  + Markdown
  + Nextcloud
  + LibreOffice
  + Mattermost
