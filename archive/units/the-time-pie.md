# The Time Pie

Attack on every lane, not just *your* favorite lane.

## Talk

![The Time Pie](images/time-pie.png)

5 minutes

In this activity, we'll describe a way to design learning units in time slices that facilitate all learning styles.

We'll do this in 4 phases.
- First, we'll **Talk** about learning styles and love languages, working set memory, conceptual / associative memory, and the Time Pie.
- Then, a **Demo** where I will draw the mnemonic grid and pie.
- Then, you'll **Pair** up with someone and make the same mnemonic grid and pie.
- Then, you'll **Share** your understanding of the most important point or two from this unit, open mic style.

How do we **learn**? Some by hearing. Some by seeing. Some by doing. Some by saying. VAKV: Visual, Auditory, kinesthetic, verbalization. Really, everyone learns all the ways, but for each learner, some ways are better than others.

For each style:
- Do you know anyone like that?
- Are *you* like that?

What is working set memory? 4-7 items

So, how can people memorize decks of cards, and complex set of switches for command-line utilities? Association. Memory castles and other mnemonic tricks, like clever acronyms and alliterative phrasings.

Conceptualization and verbalization. One of the best ways to increase retention is to ask the learner to re-teach; that is, to say in their own words, what they just learned. In order to do that, the learner must handle the objects conceptually.

## Demo

5 minutes

Draw the Time Pie

Draw the table

| Slice | Symbol  | Learner | Teacher | Who do I know that learns like this? |
| ----- | ------  | ------- | ------- | ------- |
| Talk  | :ear:   | Hear    | Say     |         |
| Demo  | :eyes:  | See     | Do      |         |
| Pair  | :pray:  | Do      | See     |         |
| Share | :lips:  | Say     | Hear    |         |


## Pair & Share

10 minutes

All students pair and do the exercises. Draw the matrix and pie. Once per learner or once per pair, at the pair's choice.

Open mic share.

## Closing Thoughts

How long is a TED talk? What is average adult attention span? The Rule of 22 for SFS Method is that if you've been talking longer than 22 minutes, you're the only one still enjoying it.

Next [How to Plot](plot.md)
