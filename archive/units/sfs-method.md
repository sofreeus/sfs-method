# SFS Method

Patterns and Anti-Patterns

## Talk

![SFS Method Overview](images/sfs-method-overview.png)

5 minutes

Welcome to SFS Method.

For the next two hours, we're going to practice patterns for planning and preparing technical training materials so that we can be better teachers.

Recognize that everybody learns slightly differently. The method attempts to cover topics in a way that all students' needs are met. We do this by:

1. Talking
2. Demoing
3. Pairing
4. Sharing

First, we're going to do the "patterns and anti-patterns" activity.

The object of this activity is to explore what makes teachers good and bad, to see if we can identify pattern behaviors (i.e. habits) in other teachers, so that we can deduce and practice positive patterns and defeat others.

We'll do this in 4 phases.
- First, this introductory **Talk**.
- Then, a **Demo** where my imaginary friend Z and I will put up *our* Patterns & Anti-Patterns, supported by memories.
- Then, you'll **Pair** up with someone and you and your pair will compose your ideas of Patterns & Anti-Patterns, supported by memories.
- Then, we'll **Share** the best of what we come up with.

## Demo

5 minutes

Open MyPaint or another drawing app.

Divide the screen into two equal halves with Patterns on one side and Anti-Patterns on the other.

Add thoughts from "Z and me".

| Patterns                   | Anti-Patterns                                   |
| -------------              | ---------                                       |
| We love trying new things. | We hate to only *hear* about cool things.       |
| - G encouraged us & gave good advice | - L went on and on and on...          |
| We love to be seen.        | We hate silence and politics.                   |
| - S gave specific, helpful feedback | - J and his cronies kept everyone in silent fear |
|                            | We hate it when the teacher is under-prepared   |
|                            | - R didn't know his topic or his material       |

## Pair and Share

10 minutes

5 minutes - pairs compose their thoughts on habits from specific memories

5 minutes - Pairs share with each other


Next [Introductions](how-to-introduce-a-person.md)
