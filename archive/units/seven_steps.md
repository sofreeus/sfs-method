- Choose a unit topic
  + tool or technique
  + definitive action statement
  + fits in an hour or less
- Plan units around activities
  What will we *do*? Why that? What common problem are we solving?
  + Plan the talk
    * What problem does the thing solve? Why might one use this thing over that?
  + Plan the demo
    * When I encounter this problem, this is how I solve it.
  + Plan the lab
    + pair and share if possible
    + *student* must do and say
    + *teacher* must (over)see and (over)hear
- Master and beyond
  + Master the Time Pie
  + Non-Time-Pie units are OK
  + Ensure that students see and hear, do and say
- Prepare for delivery
  + Schedule and announce
  + Post and send student prep check-list
    * [ ] pre-requisite knowledge
    * [ ] prep tasks
    * [ ] hardware
    * [ ] software
  + Complete teacher check-list
    * [ ] hardware
    * [ ] software
    * [ ] staging: OVA, scripts, etc.
    * [ ] rehearse talks and exercises
- Deliver! Accelerate learning!
- Gather all feedback: students, staff, and self
- Review feedback and write Issues.
