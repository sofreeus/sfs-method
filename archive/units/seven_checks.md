Run these checks on units.

Is the unit complete? Does it include hearing, seeing, doing, and saying?

---

Is the talk rational? Does it build a case for the thing? What problem does thing fix? Why is thing better than others in class?

Is the talk engaging? Am I mixing media? (voice, video, interaction) Have I planned opportunities for students to engage during the lecture?

Is the lecture as short and sweet as it reasonably can be?

---

Are my demo and the lab practically identical? If not, can I fix it?

Have I done all I can to make the student environment predictable?

Have I rehearsed my demo and tested the lab? Have I tried the lab in the student environment? Has someone else tried the lab in the student environment?

---

Bonus Questions:

Has this been simplified? Automate. Shorten. De-cruft.

Will this be engaging and fun?

Will the students learn something useful??
