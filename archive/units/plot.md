# The Plot

Who doesn't love a coherent story?

## Talk

5 minutes

![The Plot](images/plot.png)

Teachers have goals (a.k.a. objectives) for their classes.

Learners have goals, too. But, learners' goals are fuzzy and indistinct, because they're ignorant, so it is easy to beat them. Learners are happy to trade their fuzzy, indistinct goals for the clear, concise goals of a competent teacher.

But, misleading or uncertain goals from an incompetent teacher turn learners murderous.

One way to seem competent is to tell a good story, to rationalize performance goals against existential threats.

In this activity, we will choose a Topic and Objectives for your course.

We'll do this in 4 phases.
- First, we'll **Talk** about the importance of a consistent narrative and the problem of choice and what to do about it.
- Then, I'll **Demo** by introducing my imaginary friend Cloud Strife and his problem. He and I will work through choosing my next Topic and his Objectives.
- Then, you'll **Pair** up with someone and choose a topic and objectives, for each of you, or for your pair, at your option.
- Then, you'll **Share** your understanding of the most important point or two from this unit, open mic style.

## Demo

5 minutes

Cloud keeps getting flat tires on his bicycle. The roads in Midgar are full of glass and other pointy objects. The local bike shop is friendly, but he's tired of walking his bike there and paying to have them fix it. Furthermore, he notices they always slap a new tube on. Cloud is wondering if there's a way to re-use the tube.

I am an expert on flat bicycle tires. I think I know how to solve Cloud's problem. First, I'll pick a topic, then I'll pick some objectives.

## Pair

10 minutes

Pairs do likewise. Choose your next Topic and prioritize your Objectives. (Hint: Choose a simple topic that you know very well! We're going to use this in the next unit, and we won't have time to get into the weeds with anything.)

Share with everyone your topic and objectives.

## Closing Thoughts

> Now the general who wins a battle makes many calculations in his temple ere the battle is fought. The general who loses a battle makes but few calculations beforehand. Thus do many calculations lead to victory, and few calculations to defeat: how much more no calculation at all! It is by attention to this point that I can foresee who is likely to win or lose. --Sun Tzu

Next [The Prep](prep.md)
