# More like Guidelines than Rules

- Teach what you know; know what you teach
- Teach the willing
- Tell a Good Story
- Give yourself 5 * $CLASS_LENGTH to prepare
- Write units =< 1 hour
- Use the Time Pie
  * Stay on topic; don't catalogue
  * Solve a common problem
  * Test the exercise
    + in the student environment
    + on a student-like human
  * Plan for Pair & Share
- KISS Occam and Einstein

# -vvv (because 3's)

- Focus on the One Thing
  * Determine your One Thing.
  * Make sure you focus on supporting your One Thing with as many course materials as possible.
  * If a given section is not critical path for the One Thing, can it be omitted?

- Tell a Good Story
  * For the intro, emphasize the who, what, and why.
  * For the lab, emphasize the when and the how.
  * Use 3's and 7's to increase retention.

- Use the Time Pie
  * Use the Time Pie as the basic building block for a class.
  * Every unit should follow the Time Pie.
  * If it's too big for one Time Pie, it should be two (or more) Time Pies.

- Keep It Short and Simple
  * Occam and Einstein
    + When in doubt, cut it out.
    + As short and simple as possible.
    + Use the tool/technique to best effect; don't conflate simple with stupid.
  * Highlight, don't catalogue
  * Time *and* complexity must be justified. De-cruft!
  * If you think it's too simple, it's probably just right.
  * Beginners will have less to struggle with.
  * Intermediates will find the buried treasures.
  * Make images to hand out. Simplify exercises.
  * If an item seems too small to be alone, but too important to drop, bundle it up.
  * If an item seems too simple to teach, but too important *not* to teach, add it to pre-reqs and/or pre-review foundations.
  * Complexity *must* be justified. No complexity for the sake of challenge.

- Solve Common Problems
  * Common problems the general application, the better the engagement.
  * The more common the problem, the better the lab.
  * The more common the tools, the easier to learn.

- Test (and Retest) the Lab
  * Document the system requirements.
  * Does this work on my/other/all machines?
  * Test as many times as you can make time for.

- Plan for Pair n' Share
  * What will the students do for lab?
  * How can you guide them to reteach their partner?
  * What's the plan for sharing the lab results?
