Read the SFS Rap

Unit 0

SFS Method Talk (15m)

* The SFS Method is a simple framework for technical instruction that reinforces positive behavior patterns and automatically defeats common anti-patterns in technical instruction.
* The basic unit of delivery in SFS Method is the one-hour Time Pie: Talk for 15, Demo for 15, Pair and Share for 30.
  - Talk is a 15-minute lecture. ;-)
  - Demo is your chance to show how powerful and perfect it is. Make it look easy.
  - During Pair, be available, but not too available. Let students see that they can solve (many of) their own problems.
  - Encourage sharing of positive and constructive feedback.
* SFS Method is based on educational research and direct experience.
  - VAKV: Learning Styles - VAK + Maximising Retention - KV = VAKV
  - The Rule of 22: Break the monotony every 22 minutes.
  - Test your plans in a controlled student environment.
* From whence does it inherit?
  - EOTO - Each One Teach One (Human Liberation Method)
  - Parent & Child
  - Follow The Leader (Playground Method)
  - Master & Apprentice (Sith and Plumbing Method)
  - SODOTO - See One. Do One. Teach One. (Nursing School Method)
  - EDGE - Explain Demonstrate Guide Enable (BSA Method)

SFS Method Demo

How to Introduce a Panelist (Talk 5, Demo 5, Pair Play 20)

* Talk - What is an introduction? When is one needed?
* Demo - Teachers or teacher and student exchange information and introduce each other to the classroom.
* Pair - Pairs exchange information and introduce each other.

Exercise 1 (1h?)

How to plan an SFS Method Unit

* Name it "How to ..."
* Visualize the novice completing the exercise *perfectly*
* Create a pair lab.
* Write instructions for Demo and Pair. May be same or different.
* Write a Talk. What is this? When do I do this? What problem does this solve?

Exercise 2 (1h?)

How to test an SFS Method Unit

* What if the resulting Unit doesn't fit the Time Pie? It's OK if a unit doesn't adhere strictly to the formula.
  - Sometimes, Talk runs short or Demo is only a few minutes.
  - Sometimes, FTL or a group activity is a better way to exercise than Demo and Pair.
  - Sometimes, sharing is intrinsic to the pair activity, as in the Introductions example.
* Ask some questions:
  - Am I asking too much of the students? Too much material? Too hard? Too many context switches? Too long in-phase? 22 minutes max!
  - Is there any obvious cruft to remove? Pointless choices? Useless information?
  - Does this unit meet the needs of VAK learners?
* Get a friend, or an extra laptop.
* Follow the plan. Build in fixes for discovered failure modes.
* Re-test until no new failure modes are discovered.

Exercise 3 (4m)

How to deliver an SFS Method Unit or Course

* Follow the plan
* Expect unanticipated questions, events, and failures. Be ready to accept and adapt.

Exercise 4 (1h)

How to improve an SFS Method Unit

* Gather feedback
* Re-write from scratch? If it's been delivered less than 3x, probably.
* Add missing bits
* Remove extraneous bits

To Do:

Write several fictional SFS Method Learning Units:
- Palpatine teaches How To Defenestrate a Jedi
- Julia Child teaches How To Bake an Apple Pie
- Hephaestus (Vulcan) teaches How To Forge a Lightningbolt

Finish writing "How to build an SFS Course"
1. import the course plan template:
  - attributes: maintainer, topic, length, plan (this)
  - requirements (hardware, software, knowledge)
  - exercises/
methods:
    + planning
    + promotion
    + delivery
       Read the SFS Rap

       Gather rewards and feedback for one week
2. fill it out
3. check it in to a new class repo
