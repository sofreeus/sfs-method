# SFS Method

A pattern for creating technical training materials which helps to prevent common problems.

0. [wrapper](wrapper-slides.fodp)
1. [The Time Pie](LABS/time-pie.md)
2. [The Plot](LABS/plot.fodp)
3. [The Prep](LABS/prep/)
4. Credits and Feedback

Is there anything about the problem being solved, or the particular *way* it's being solved, or the topic or objective that is *both* relevant and important? That goes in the Talk. And, it's a knowledge Objective where everything *else* is a performance Objective.

Now you have an Exercise. The exercise needs to be do-able in the student's learning environment. Create the student's learning environment and do the exercise, ideally, with a partner.

Now, write Requirements. Ideally, there's only one: Docker, Virtualbox, bash, a browser, or whatever, but maybe there are two or three or ten.

Now, write a lesson plan. You don't have much of a Talk, if any, yet. So, consider a few points like: What is it? What problem does it solve? Is it better than other things in it's class? If so, how? What *problem* does it solve?

Rehearse your Talk. Rubber ducky is good. Human partner is better.

Set time after the student's do the Exercise in a Pair to share a bit about the experience, and to review important knowledge Objectives.

Speaking of which, let's review: What does the participant learner do, during the unit? They hear, see, do, and say. What does the teacher do? They talk, demo, then they shut the Hell up and guide and enhance the pair and share.

CALL OUT YOUR PHASE TRANSITIONS!

Can you do all this in less than an hour? If you can, you can teach a one-unit class. Can you do all this four, five, or six times? If you can, you can teach a half-day class.

[Class Materials Template](https://gitlab.com/sofreeus/class-template)
[CONTRIBUTING](CONTRIBUTING.md)
[REQUIREMENTS](REQUIREMENTS.md)
[OBJECTIVES](OBJECTIVES.md)
[APPENDIX](APPENDIX.md)
