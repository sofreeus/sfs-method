#!/bin/bash -eu

# Download the Reveal.js framework to the public artifact directory
git clone https://github.com/hakimel/reveal.js.git public
# Check out version 3.4.1 (latest release when this was authored)
(cd public && git checkout tags/3.4.1)
# Copy our content to the pages directory
cp index.html public
cp -R slides public/
cp -R images public/
