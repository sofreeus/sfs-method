# SFS Method

Note: This method is specific to technical teaching.  It may be applicable elsewhere, but it comes with a money back guarantee for technical topics.


## The One Thing

All classes should have a "One Thing."

This class's One Thing is:

    ..................,::::,................
    .............,:~====++===~,.............
    ..30m.....:~=======:~++++==~:.....15m...
    .......,~====~~~==++++++++~:+=~,........
    .....:~===~~==~~:~+~=:~~==+++:++=:......
    ...:~===~=~~::...~=~=:...,~=+++=++=:....
    ..:===~=~~,......~=~=:......:=++=+++:...
    .:===~~:,........:=~=~........:=++~++,..
    .~==~~:..........,=~=~....,.....:=+:+=,.
    :==~~:............~~=~...TELL,,..:=+~=:,
    =+~=~.............~==:.............=:+~,
    =+=~,......,,....,~==~.............~==~:
    =~=~...PAIR.,,....~==~,......,,,,::=+~=:
    =~.....PLAY.,..,...~+=~~~~=======++++~=:
    =,.......,.,,......~++=============++~=:
    =~~:..............:~==~:::::::,,,,:=+~~,
    =++=~............:=~=~:.....,.....~+++~,
    =+~==:...........:=~=~,..SHOW....:=+++:.
    ~=~==~,..........,=~=~,......,..:=+++=:.
    :==~=~:..........,=~=~.........:=++==~..
    .:==~=~:........,=+~=~......,:~=++++=,..
    .,~=+~=~:,......~=+~==...,:~~==+++==:...
    ..,~=+~==~~:::,,~++~+=~~~~~==++++=~,....
    ....:==+~======~=++~++===~+=+++=~:......
    .....,:~======~=~~~~++++++===~~,...15m..
    .........,:~~===========~~:,,...........
    ........................................

1. Talk
  - Talk for 15m or less
  - The average adult attention span is 22m
2. Demo
  - Show this lesson's topic
  - Time box to 15m
3. Do
  - Pair Play (Pair & Share)
  - Have students do the lab, and teach the lab to their partner
  - Mix in group-wide sharing (as applicable, and as time permits)
  - Time box to 30m

## Lessons

The One Thing only accounts for an hour of time.  Longer classes should be broken down in to one hour lessons that follow the format of the One Thing.

The class - and each lesson within - should follow answer the following three questions:
- *What?*  What it is.  What it does.
- *Why?*  Why should it be used?
- *Who?*  Who benefits from using it?

Put another way:
- *Theory* behind the technology.
- *When* it should be used.
- *How* to use/employ it.

*Trim down supporting arguments to highlight the main point.*

## How do we learn?

- Didactic vs. autodidactic: hearing/seeing/doing
- Kinetic learners (fidgety)
- Verbalizing (reteaching)

## Sevens & Threes

- If you have a list of 1,000,000 things, trim it down to the 7 most important.
- If you need to trim it down further, trim it to 3.
- If you have to pick the most important thing, pick from the 3.
- N -> 7 -> 3 -> 1
- https://en.wikipedia.org/wiki/Rule_of_three_(writing)
-  http://www.forbes.com/sites/carminegallo/2012/07/02/thomas-jefferson-steve-jobs-and-the-rule-of-3/#1cf23dfdd04f
- http://www.copyblogger.com/rule-of-three/

## Value Ramping

- Lead with the highest-value thing first
  - Sometimes an operational issue or dependency will break this
  - Try to make the dependencies smaller


- Err on the side of too simple
- Err on the side of too short
- Err on the side of too small
  - The only person who will lament the lack of complexity is you, the teacher
  - Students looking for more complexity can ask for it (during lab or otherwise)

Structure lessons in terms of decreasing value in order to cut your losses if you can't cover everything.

    |
    |_
    | |
    |L|_
    |e| |  _
    |s| |_| |  _
    |s| | | |_| |
    |o| | | | | |_   _
    |n| | | | | | |_| |_ _
    | | | | | | | | | | | |_
    |1|.|.|.|.|.|.|.|.|.|N| |_
    |_|_|_|_|_|_|_|_|_|_|_|_|_|__
