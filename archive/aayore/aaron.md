# SFS Method

A class teaching you to teach classes. <!-- .element: class="fragment" data-fragment-index="1" -->

---

### What is SFS Method?

A method for developing and delivering technical classes <!-- .element: class="fragment" data-fragment-index="1" -->

Note:

This is a class about developing and delivering classes

So we're going to develop and deliver (parts of) a class.

The first few lessons might seem a bit backward, but they'll make sense shortly.

This is not a purely qualitative/technical class --> difficult to present with SFS Method

We'll stick with SFS Method as much as possible (3 lessons), but there's quantitative stuff at the end that doesn't fit so well.

Survey the room to see who has a class topic.

---

### Lesson 1: Introductions

Theory <!-- .element: class="fragment" data-fragment-index="1" -->

Why <!-- .element: class="fragment" data-fragment-index="2" -->

Who <!-- .element: class="fragment" data-fragment-index="3" -->


Theory: What is it for?  What does it do?

Why would someone do/use this?  What are the benefits?

Who is the right person to own/drive this?


Note:

- Theory behind the technology.  The philosophy inherent in the thing you're teaching.

- Why it's better than the alternative(s).  This might blend with theory.

- Who is the right person to own/drive this?

---

### Demo 1: Introduction to SFS Method

Because non-experts <!-- .element: class="fragment" data-fragment-index="1" -->

who don't care <!-- .element: class="fragment" data-fragment-index="2" -->

teaching non-experts <!-- .element: class="fragment" data-fragment-index="3" -->

who don't care <!-- .element: class="fragment" data-fragment-index="4" -->

(and talking too damn much) <!-- .element: class="fragment" data-fragment-index="5" -->

*doesn't work.* <!-- .element: class="fragment" data-fragment-index="6" -->


But there are experts

who care deeply <!-- .element: class="fragment" data-fragment-index="1" -->

and want to teach <!-- .element: class="fragment" data-fragment-index="2" -->

non-experts <!-- .element: class="fragment" data-fragment-index="3" -->

who want to learn. <!-- .element: class="fragment" data-fragment-index="4" -->

But that doesn't always work, either. <!-- .element: class="fragment" data-fragment-index="5" -->


Experts need a framework, so they can teach without becoming education experts.


### Review

Theory: Experts need a framework, so they can teach without becoming education experts. <!-- .element: class="fragment" data-fragment-index="1" -->

Why: Because smart people can teach bad classes, even if they know the material. <!-- .element: class="fragment" data-fragment-index="2" -->

Who: People who have something to share, but lack teaching experience. <!-- .element: class="fragment" data-fragment-index="3" -->

---

### Lab 1: Introduce Your Class

Pair up <!-- .element: class="fragment" data-fragment-index="1" -->

Each of you come up with a class idea <!-- .element: class="fragment" data-fragment-index="2" -->

Theory - Who should do it - Why it's good <!-- .element: class="fragment" data-fragment-index="4" -->

Note:

Get up in front of the class and introduce your class.

---

### Lesson 2: Lesson Structure

Each lesson has a very clear structure.


**Lesson Step 1**

"What," "Why," and/or "When" are presented as a 15 minute lecture.


**Lesson Step 2**

"How" is shown as a 15 minute demo.


**Lesson Step 3**

Students pair up, complete a lab, and reteach.

- Always reteach to partner <!-- .element: class="fragment" data-fragment-index="1" -->

- Reteach to class if possible <!-- .element: class="fragment" data-fragment-index="2" -->


<img src="images/time_pie.png">

Note:

I will hammer this in to your brain.  Mercilessly.

---

### Demo 2: Lesson Structure


<img src="images/time_pie.png">


### What does this lesson structure do for students?

Addresses varied learning styles. <!-- .element: class="fragment" data-fragment-index="1" -->


Didactic vs. Autodidactic

Visual Learners<!-- .element: class="fragment" data-fragment-index="1" -->

Kinetic Learners <!-- .element: class="fragment" data-fragment-index="2" -->

Verbalizing <!-- .element: class="fragment" data-fragment-index="3" -->

Note:

Didactic: Instructor-led learning

Autodidactic: Self-led learning

Visual: Learning by watching

Kinetic: Learning while moving; Some people learn better when they fidget

Verbalizing: Restating what you've learned forces you to understand a topic well enough to explain it in terms you understand.

Hearing/seeing/doing - This method covers most of the bases pretty well.


Time Pie from a Teacher's Perspective

Tell <!-- .element: class="fragment" data-fragment-index="1" -->

Show <!-- .element: class="fragment" data-fragment-index="2" -->

Advise & Assist <!-- .element: class="fragment" data-fragment-index="3" -->


<img src="images/time_pie.png">


Time Pie From a Student's perspective

Hear (didactic) <!-- .element: class="fragment" data-fragment-index="1" -->

See (visual) <!-- .element: class="fragment" data-fragment-index="2" -->

Do (kinetic, autodidactic) <!-- .element: class="fragment" data-fragment-index="3" -->

Tell (verbalizing) <!-- .element: class="fragment" data-fragment-index="4" -->


<img src="images/time_pie.png">


### What does this structure do for teachers?

Simplifies class planning. <!-- .element: class="fragment" data-fragment-index="1" -->

Provides a planned, one-hour lesson. <!-- .element: class="fragment" data-fragment-index="2" -->

Create your entire class based on these one-hour chunks. <!-- .element: class="fragment" data-fragment-index="3" -->


### Review

What: Lesson Plans / Time Pie

Why: Attention Spans / Learning Styles

When: Creating a class / Planning lessons


What will students learn in this lesson?

Why is this lesson important?

When do you need to do this?

---

### Lab 2: Lesson Structure

In pairs... <!-- .element: class="fragment" data-fragment-index="1" -->

Come up with a lesson that supports your class. <!-- .element: class="fragment" data-fragment-index="2" -->

2-3 minute summary of What/Why/When <!-- .element: class="fragment" data-fragment-index="3" -->

2-3 minute How/demo <!-- .element: class="fragment" data-fragment-index="5" -->

Note:

Keep it high level.

Teach your lesson to the class.


<img src="images/time_pie.png">

---

### Lesson 3: The One Thing

If students only remember one thing about your class, it should be this One Thing. <!-- .element: class="fragment" data-fragment-index="1" -->

It doesn't have to be a standalone lesson (but it might be). <!-- .element: class="fragment" data-fragment-index="2" -->

Reinforce the One Thing with every lesson. <!-- .element: class="fragment" data-fragment-index="3" -->

Trim down supporting arguments to focus on the main point. <!-- .element: class="fragment" data-fragment-index="4" -->

---

### Demo 3: The One Thing


<img src="images/time_pie.png">


It helps teachers plan lessons and classes.

It accomodates a variety of learning styles.

Note:

Our theory: Experts with a simple teaching framework can be great teachers.

Who: Anyone with any level of expertise to share.

Why: Because not having expertise or a framework makes for lousy classes.

---

### Lab 3: The One Thing

Pair up <!-- .element: class="fragment" data-fragment-index="1" -->

Identify the One Thing for your class <!-- .element: class="fragment" data-fragment-index="2" -->

Help your partner focus on a succinct rationale for the One Thing <!-- .element: class="fragment" data-fragment-index="3" -->

How would you add this to the lesson you delivered in the last section?  <!-- .element: class="fragment" data-fragment-index="4" -->

Note:

Get up in front of the class and communicate your One Thing.

---

### Simplicity

Err on the side of too simple <!-- .element: class="fragment" data-fragment-index="1" -->

Err on the side of too short <!-- .element: class="fragment" data-fragment-index="2" -->

Err on the side of too small <!-- .element: class="fragment" data-fragment-index="3" -->

Note:

The only person who will lament the lack of complexity is you, the teacher

Students looking for more complexity can ask for it (during lab or otherwise)

---

### The Time Pie

<img src="images/time_pie.png">


### Class Planning

Plan a Time Pie for each lesson <!-- .element: class="fragment" data-fragment-index="1" -->

You'll have a rough plan for the number of hours your class will take <!-- .element: class="fragment" data-fragment-index="2" -->


### Adjustments

Adjustments are okay <!-- .element: class="fragment" data-fragment-index="1" -->

Short lectures/lessons are fine <!-- .element: class="fragment" data-fragment-index="2" -->

Average adult attention span is 22 minutes <!-- .element: class="fragment" data-fragment-index="3" -->

DO NOT go over 22 minutes <!-- .element: class="fragment" data-fragment-index="4" -->

If 22 minutes isn't enough, break your lesson in two <!-- .element: class="fragment" data-fragment-index="5" -->


### Logistics

Space <!-- .element: class="fragment" data-fragment-index="1" -->

Prerequisites <!-- .element: class="fragment" data-fragment-index="2" -->

Tools <!-- .element: class="fragment" data-fragment-index="3" -->


Note:

Space:
- What resources are available?
- Sitting/standing
- Desks vs. couches

Prereqs:
- Self-serve?  You need to help?
- How to communicate w/ students?

Tools:
- Whiteboards?  Projection?
- How will students demo their work?

---

### Story Mode

Write the tech first <!-- .element: class="fragment" data-fragment-index="1" -->

Humanize the tech <!-- .element: class="fragment" data-fragment-index="2" -->

Solve common problems <!-- .element: class="fragment" data-fragment-index="3" -->


### Common Problems

The more common the general application, the better the engagement. <!-- .element: class="fragment" data-fragment-index="1" -->

The more common the problem, the better the lab. <!-- .element: class="fragment" data-fragment-index="2" -->

The more common the tools, the easier to learn. <!-- .element: class="fragment" data-fragment-index="3" -->

---

For the love of all that's good and pure...

TEST YOUR LABS! <!-- .element: class="fragment" data-fragment-index="1" -->

THEN RETEST YOUR LABS! <!-- .element: class="fragment" data-fragment-index="2" -->

THEN RETEST YOUR LABS AGAIN! <!-- .element: class="fragment" data-fragment-index="3" -->

Note:

Rule of thumb: 4-5x the planned class length preparing the class.

---

### Patterns and Anti-Patterns

Don't focus on "not doing" the anti-pattern. <!-- .element: class="fragment" data-fragment-index="1" -->

Study and understand the value of the pattern <!-- .element: class="fragment" data-fragment-index="2" -->

then teach it whole-heartedly. <!-- .element: class="fragment" data-fragment-index="3" -->

---

### Value Ramping

Lead with the highest-value thing first  <!-- .element: class="fragment" data-fragment-index="1" -->

Sometimes an operational issue or dependency will break this <!-- .element: class="fragment" data-fragment-index="2" -->

Try to make the dependencies smaller <!-- .element: class="fragment" data-fragment-index="3" -->


Structure lessons in terms of decreasing value

in order to cut your losses

if you can't cover everything


Value of Lessons
<pre>
|
|___
|   |
| L |___
| e |   |    ___
| s |   |___|   |    ___
| s |   |   |   |___|   |
| o |   |   |   |   |   |___     ___
| n |   |   |   |   |   |   |___|   |___ ___
|   |   |   |   |   |   |   |   |   |   |   |___
| 1 | 2 | 3 | . | . | . | . | . | . | . | N |   |___
|___|___|___|___|___|___|___|___|___|___|___|___|___|__
</pre>

---

### Sevens and Threes

If you have a list of 1,000,000 things, trim it down to the 7 most important.  <!-- .element: class="fragment" data-fragment-index="1" -->

If you need to trim it down further, trim it to 3.  <!-- .element: class="fragment" data-fragment-index="2" -->

If you have to pick the most important thing, pick from the 3.  <!-- .element: class="fragment" data-fragment-index="3" -->


N -> 7 -> 3 -> 1

Note:

https://en.wikipedia.org/wiki/Rule_of_three_(writing)

http://www.forbes.com/sites/carminegallo/2012/07/02/thomas-jefferson-steve-jobs-and-the-rule-of-3/#1cf23dfdd04f

http://www.copyblogger.com/rule-of-three/

---

### Thank you

Questions?  Comments?  Concerns?  Feedback?
