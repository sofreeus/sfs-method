### Instructions for running a local copy:

    docker build -t revealer .
    docker run -d -p 10101:8000 --name revealer -v $(pwd)/slides:/reveal.js/slides -v $(pwd)/images:/reveal.js/images revealer

After you do this, your slides should be available at [http://localhost:10101](http://localhost:10101)

You shouldn't have to update your `index.html` too often.  But if you do, rebuild your container:

    docker stop revealer
    docker rm revealer
    docker build -t revealer .
    docker run -d -p 10101:8000 --name revealer -v $(pwd)/slides:/reveal.js/slides -v $(pwd)/images:/reveal.js/images revealer
