## SFS Method Objectives

By the end of SFS Method, the participant learner will:

How and why to:

1. Use the Time Pie: Talk, Demo, Pair and Share
2. Plot a class: Goal, Exercises, Story(?), Talk/s, Unit Outlines
3. Prep for delivery: WRWRW:D

We may also cover:

- Use the 7/3/1 system for making and prioritizing lists
- Arranging units within a multi-unit classes
- Choosing and using an exercise environment
- [SFS Method Materials](https://gitlab.com/sofreeus/sfs-method) and [Class Template](https://gitlab.com/sofreeus/class-template)
