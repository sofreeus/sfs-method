# The SFS Method for Teaching #

_Draft by Bernardo Trindade v0.2_

The goal of this method is to outline how to make a course that fits in the [Software Freedom School](http://sofree.us)
approach to teaching. The SFS philosophy is centered around using open source software solutions and enabling anyone and everyone to use
them to their fullest potential. SFS courses are designed to put the student to work and have them learn by doing and enable them to explore
the software they are learning. As instructors it is not always easy to design interactive courses, especially since most of us never
had formal training to be educators. This document hopes to provide some insight into how to think about teaching your course so that you
can more readily define your course and what activities belong in your course. For the sake of this document, we will use the example of
teaching a class on how to use the text editor Vim. 

## Preparing for the class ##
Many teachers will say to prepare for a class they will write a lesson plan, proof read their workshops, and prepare their slideshows.
Those are mechanisms for teaching a course, and especially when teaching software those mechanisms will vary based on the topic. Instead we
will focus our preparation on the more abstract. How do we specify what we want to teach? What should we cover? What should we focus on? The
following sections are meant as guiding questions to make a curriculum that covers everything you, the expert, believe is important for a
beginner to learn.

### What do you want to teach? ###

Before desiging your course, ask yourself what is it you want to share with your students? What is one subject in which you have knowledge
that others may not have? By the end of the day what should someone who took your class be able to do?

Your course should teach a new concept, or help others use a new software tool. It is very easy to become side tracked, to begin teaching
one thing and become sidetracked by another subject, but by specifing exactly what your course should teach over the course of a few hours
it is much easier to stay focused and have a successful class. Once you've defined the core goals, you can have secondary objectives that
you'll get to if you have time or that you can recommend to your students to study in the future.

Example: I want to teach people how to use Vim. I want them to able to use the Vim keybindings comfortably and be able to open and write
their own files. The core functions they should learn are:

* How to open Vim
* How to leave Vim
* How to type (insert mode)
* How to go back to command mode
* How to save a file
* How to save a file under a different name
* How to move (up, down, left, right, top, bottom)
* What is considered a "word"
* How to delete (character, word, line)
* How to replace
* How to copy and paste
* How to search a file
* How to find help pages

At the end of this class, everyone should be able to perform these tasks. If possible I'd also like them to be able to open and edit
multiple files, learn how to customize their vimrc, learn how to customize their keybindings, and learn how to create on the fly macros.

### Why do you want to teach it? ###

Why does this particular subject help somebody? Why should someone choose this tool over other similar tools? Once someone has taken your
class what will they use these skills to do?

It is easy to focus purely on the "How" when learning software, but it is important to also address the "Why?".
The goal of this question is to properly define why your class is necessary. What aspect of people's knowledge is currently lacking that
will benefit from your course? A fantastic example of the "why" is the SFS Hands-on introduction to Puppet. There are plenty of
administrators who will connect to a server remotely and perform all necessary configurations by hand. By learning how to use Puppet, how
are their lives improved? How can Puppet be used to perform their daily tasks more efficiently and more robustly? How does learning this skill
set make them better administrators? This will all vary widely based on what kind of software you are teaching, but the goal of
your class should be to address some need that you see. The people who come to take your course should leave with the skills to
improve some aspect of their knowledge that was previously lacking and to leverage these newfound skills to solve problems. But they can
only do that if you teach them how the software you're teaching them can meet that need.

#### When should I use this? ####

This is still largely a subset of the question "Why do you want to teach this software?", but is an important enough point that it warrants
it's own section to discuss it. The famous perl motto TIMTOWTDI (there is more than one way to do it) applies to much of the software world.
For any one problem, text editors for example, there are always multiple different solutions. Your students may or may not know about the
alternatives, but as an instructor it is a good idea to address their pros and cons. Why should someone learn to use one of these tools, and
why is the one you're teaching the best one?

As the instructor, you've explored (or should have familiarity) with several tools that address this problem, and you have decided that this
one works best. Share your knowledge with your students, what is it about the tool you've chosen that makes it better than the others?
What are the advantages over the alternatives?

Example: Why should people learn console-based text editors? Any unix system will have a terminal, and a text editor available while they
may not always have a GUI. By learning console based text-editors we become more powerful because we can always create or edit a file even
if we are working remotely and don't have a GUI. Vim in particular gives the user a lot of power to perform tasks quickly in efficiently
while editing a document. Once the user is comfortable with Vim, they can quickly make changes, perform repeated tasks, and add new content.
Also by learning the Vim configuration the user can create their own development environment, customizing Vim to meet their unique needs and
help them work more efficiently and more comfortably. While there are many text editors with many pros and cons, Vim is one of the most
intimidating and new users may not be willing or able to approach Vim. The goal of this course is to help newcomers to Vim get over the
intimidation factor and see what Vim has to offer so that they can make an informed decision as to whether or not they want to use it.

I see Vim much the same way I see playing piano. At first it's very intimidating, there are lots of keys that if not hit properly will lead
to absolute chaos. But once you've been shown the basics you can do so much more than other instruments because of those keys you once found
so scary. However, to avoid the inevitable flame war, E-macs is very comparable to Vim. They are both highly programmable and customizable.
I prefer Vim because it is a modal editor, I can control it completely with minimal keystrokes and I prefer to work in "modes" based on how
I want to edit. E-macs is a more functional editor, where different functions can be set to unique keystrokes and users can customize which
functions they use more often to be more accessible (https://xkcd.com/378/). As far as speed between the two, I think it depends on the
user. Whichever you're more comfortable with will be easier to use and therefore faster.

---

## Teaching the class ##
Once you know what you want to cover, now comes the fun part. The purpose of the class is to take people who have never touched this
software before and get them comfortable enough to use it from now on. From the section above we have a list of features we want to teach,
and we know when we should use these features. Each of these items becomes a topic, or a "chapter" of the class. Typically a class will have
3-7 chapters depending on how much content fits into each chapter. Too few chapters and students don't feel like they learned enough, and
too many and it's too much for a student to properly absorb.

For example in our Vim class, a good breakdown of the features we've narrowed down would be around 5 chapters.

1. Opening files (how to make a new file, open an existing, saving, and using insert mode)
2. Navigation (how to move, using "words", searches)
3. Manipulation (replace, delete, new line, copy, paste, etc)
4. Macros _Note: this is a lower priority. If I'm running low on time, I'll mention this but move on without digging into it_
5. Customization (text highlighting, new keybindings/commands, installing plugins)

Think of the class as a for loop.
```
for topic in chapters do
	introduce topic.features
	use topic.features when (conditions)
	demonstrate topic.features
	do_share()
end
```
Each topic can be broken down into this general structure to make sure that the students have a strong understanding of one topic before
moving on to the next. The cycles may not be of uniform length, for example in our Vim example the first topic will likely be fairly quick
whereas the second may take a bit longer. This is not a strict structure, but a guideline to help you structure your classes as cycles
focused on hands-on exercises instead of lecturing.

### Introduce Features ###
This is where you first explain what this topic is going to be about. If applicable, this is where you'd give any background or explanation
about the tool and what the feature is. Ideally any questions the student may have about the feature itself should be answered here. What is
it, what does it do, why does it work this way, etc. Try to keep this section as short as possible, just enough so that students understand
what they're doing and aren't blindly following along. If your topic has a lot of philosophy behind it then consider holding a discussion
afterwards during a break and focus on the hands on aspect of the feature. For example, Puppet can be very philosophical in the "right" way
to manage an infrastructure, system design, etc., but for the class it's better to just focus on how to use puppet and save those
discussions for after class when everyone has already done the exercises.

For our Vim example, I'd explain that Vim works based on "modes" determined by keystrokes. Once Vim starts you are in "command" mode so you
can't type right away.

### Explain when Features are Used ###
This goes hand in hand with the introduction. Explain when to use this feature, and what circumstances would tell you that this is the
feature you want to use.

For our Vim example, I know I want to open a new file, type "Hello World" and save it as hello_world in my home directory. Then show them
how to open the file again, and add a new line saying "Hello again", and saving it.

### Demonstrate Features ###
Once you've explained the feature (or as you're explaining if you feel that is more helpful) demonstrate the feature. This is where you show
your students how to actually use this part of the tool and watch what it does.

For our Vim example, this would be where I open a new file and show them that typing doesn't put characters on the screen. I also show them
that I hit "esc", and then hit the letter "i" to go into insert mode before saving and quitting.

### Do/Share ###

This is the real core of an SFS class. Now that the students know what they're doing, why, and how, you have them actually do it. This is
the "Doing" portion where students get hands on experience. Once students have a handle on the task, have them partner up and work through a
few scenarios using the circumstances you explained. By working together they'll be able to watch each other to catch mistakes and come up
with new ideas. This is the "Sharing" portion where students bring their different experiences to your software and explore it as they
learn. They can ask questions of you while also learning about the software and how it can be used by collaborating with someone else.

In our Vim example, this is where you would have people partner up and practice with opening files and using insert mode. If any one is
having trouble their partner will be there to help point out a mistake they're making and you can focus on answering any outlying questions.
