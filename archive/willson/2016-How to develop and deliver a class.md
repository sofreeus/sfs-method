Leader tips
===========

Writing the outline
-------------------
1 Topic, One Story-Line "Puppet makes massive admin fun"
3 Step Loop
  A Describe
  B Demonstrate
  C Delegate(pair-oriented lab)
7 Real-world Loops (each with talk, demo, pair)

Infrastructure
--------------
Have a repository for code and documents (DLW likes gitorious.org) Join IRC or BBB or whatever

Planning
--------
How much space does each student need?
What gear will this class need?
  - wired network?
  - wireless network?
  - sticks?
