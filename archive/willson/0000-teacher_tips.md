Please read through the SFS_Method material and/or take the class.

Show & Tell, Advise & Assist: Some learn by hearing. More by seeing. Almost all by doing. Doing > Seeing > Hearing. Good teachers help learners become capable "do'ers". Not all bad listeners are bad learners.

A learner who expects to help another learner will strive for comprehension. The best teacher makes more teachers.

Remember: There is no "teaching". There is only learning. You can help learners learn, or you can get in their way. Be a guide to the eager learner.


Start with a bang!
---

Jump right in! Do something amazing! Don't start with history. Paying attention to history requires a deeper love than your students have yet.

Why bother?
---

What problem are we trying to solve? What does (this) do, better than anything else?

Get to the point(s)!
---

Put your learners on an "objective-based learning" course.

What would you want your assistant(s) in (this) to know, before you die and they have to take over? Those are your objectives.

Finale with a capstone
---

Let the students prove to themselves that they've learned something of use.

Finish with: Would you like to know more?
---

This is where your link to the fascinating history of (this) goes.

