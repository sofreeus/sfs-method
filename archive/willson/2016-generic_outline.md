I would call it "A Gentle Hands-on Introduction to X "

I would start with a 15 minute show-and-tell: "What's so great about X?"

I would then do four or so one-hour "When to / How to" chapters of the following form:
5m S is a cool feature of X.
5m You use this feature when P.
10-15m This is how you use S (demo).
30m Try that.

For OwnCloud, the chapters might be something like:
OwnCloud. (Install)
OwnCloud Clients. (Install)
Users and Groups.
Shared Folders.
Shared Folders between users on different servers.
