To be fully prepared to participate in the SFS Method class, do three things:
1. Get your favorite note-taking tool ready. Mine is a sketch pad or hex pad and colored pens.
2. Remember an experience of poor teaching: When did you want to learn, but the teacher didn't make it better?
3. Remember an experience of good teaching: When did you learn a ton and have some fun, at the same time?
