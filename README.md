# SFS Method

The SFS Method is a framework for teachers that aids in the preparation, delivery, and efficacy of technical training.

In order to accomplish her goal of expanding her nascent chain of coffee shops, Daphne needs to develop training for her managers.  We're going to use the SFS Method to help craft her class.

---

### Lessons

1. [Theory of SFS Method](/units/unit_1.md)
1. [The Time Pie](/units/unit_2.md)
1. [Flow](/units/unit_3.md)
1. [Prep](/units/unit_4.md)

---

### Credits

* [BSA EDGE Method](credits/BSA-EDGE-Method-with-Rob-Polocz.wav)
* [EOTO - Each One Teach One](credits/Each_One_Teach_One-George_Saylor.3gp)
* SODOTO - See One Do One Teach One

### Extras

- [archive](archive/) of old Method materials
- [tools](tools/) for building class mats

---

[CONTRIBUTING](CONTRIBUTING.md) | [REQUIREMENTS](REQUIREMENTS.md) | [OBJECTIVES](OBJECTIVES.md)
