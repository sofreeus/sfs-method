How to Record A Screencast
===

Use recordmydesktop or one of its GUI counterparts

    $ gtk-recordMyDesktop
    $ qt-recordMyDesktop
    

Last time, I had trouble figuring out my audio device, when neither 'DEFAULT' nor 'default' worked. After a LOT of searching, I found this command to output a list of the ALSA/OSS devices available on my system.

    $ arecord -l

Ultimately, I found that for me, the CLI version of recordmydesktop was more effective, both for testing and for the actual recording.

    $ recordmydesktop --fps=10 --device hw:3,0 --on-the-fly-encoding --delay=10 -o router

After I did my recording, I transcoded the file to reduce file-size and maximize portability.

    $ ffmpeg -i router.ogv router.webm

