VirtualBox Screencast Script
===

by David L. Willson for SFS, 2015, CC BY SA

We use VirtualBox in lots of SFS classes. This is how to install it.

I'll install VirtualBox using the virtualbox.org apt repository. This is the best way to install on Debian and Ubuntu. Fedora, RHEL, CentOS, and Scientific Linux would use the yum repo, instead. Windows and OSX would install from their respective binaries and do periodic updates manually.

More info: https://www.virtualbox.org/wiki/Linux_Downloads

In Ubuntu ...

```bash
wget -q https://www.virtualbox.org/download/oracle_vbox.asc &&
sudo apt-key add oracle_vbox.asc &&
rm oracle_vbox.asc
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc &&
sudo apt-key add oracle_vbox_2016.asc &&
rm oracle_vbox_2016.asc
lsb_release -c
lsb_release -c | cut -f 2
echo "deb http://download.virtualbox.org/virtualbox/debian $( lsb_release -c | cut -f 2 ) contrib" |
sudo tee /etc/apt/sources.list.d/virtualbox.list
sudo apt-get update
sudo apt-get install virtualbox...
```

In openSUSE Leap 42.1 ...

```bash
sudo zypper install virtualbox
```


How to make a CentOS 7 minimal image in VirtualBox
---

An image ( or base VM ) is a good way to avoid repetitive tasks that are not unique to the machine, especially when the SA plans to have *lots* of the same kind of machine.

Instance machines from an image must be unique-ified. Examples: MAC addresses of instances must be unique if instances will need to be on the same Ethernet segment. hostnames should be unique to help the user distinguish between instances, and ssh host keys should be unique to avoid a security vulerability.

screenkey is useful to show key combinations that would otherwise be invisible

VirtualBox allows differential clones which are faster to create and take up less space by referring to their source for all unchanged storage blocks.

### base VM build process

* Download and verify the latest CentOS 7 Minimal ISO
* Note: During demonstration, consider screenkey
* Default install C7-base from the ISO
* Note: No need to set root password if you set first user
* Install guest extensions
* Fully update the base VM
* Shut down the base VM
* Take a snapshot
* Create a few differential clones from the snapshot
* Set VM names and hostnames as you go
