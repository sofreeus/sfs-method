# SFS Workstation VMs

SFS will publish OVAs for VirtualBox, as a component to reliable and consistent exercise environments.

The archives are available at our NextCloud. (see link)

We will use the leading edges of the distributions LF supports in their certified sysadmin exam.

Currently:
- Ubuntu
- Fedora
- openSUSE

The following are verified on each machine before release:
- osadmin has a standard password and is a global sudoer
- up-to-date with patches
- rebooted since last patch
- screen resizes automatically with window
- copy and paste works bi-directionally with the host
- sshd is not enabled
- machines are otherwise default installs

TODO:
  - git-bits (fetches all this)
  - cab (creates users Alice and Bob as global, password-less sudoers)
  - stouith (Shoot The Other Users In The Head, deletes Alice, Bob, and osadmin, except for whoever's running the script)
  - git-a-class (does whatever's needed to install a class to ~/git/some-class)

- Until the above is done, do this:

```bash
wget https://www.sofree.us/bits/git-a-class
chmod +x git-a-class
./git-a-class $CLASSNAME
```
[More about Learner Stations at The SFS303 Project](https://gitlab.com/sofreeus/sofreeus/blob/master/sfs-learnstations.md)
