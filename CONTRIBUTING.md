Anyone may submit Issues or Merge Requests.

These things will give your patches, suggestions, and questions more noble cromulence.

- Attend SFS Method 1-3 times, preferably by different teachers.
- Understand thoroughly the what/why/how of the Time Pie, 7/3/1, Plot, and Prep.
- Deliver Method once.
- Teach something other than Method a few times.
- Learn something from a teacher several times.
